public class Mutual {
    public static boolean isMutual(int n1, int n2, int n3) {
        int nod = 1;
        for (int i = 1; i <=Math.abs(n1) && i <= Math.abs(n2) && i <= Math.abs(n3); i++) {
            if (n1 % i == 0 && n2 % i == 0 && n3 % i == 0) {
                nod = i;
            }
        }
        return nod == 1;
    }

    public static void main(String[] args) {
        System.out.println(isMutual(12, -9, 900));
    }
}


