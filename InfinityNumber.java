public class InfinityNumber {
    private static boolean increaseNext = false;

    public static String sum(String n1, String n2) {
        StringBuilder sb = new StringBuilder();
        String smallStr = n1.length() > n2.length() ? n2 : n1;
        String bigStr = n1.length() > n2.length() ? n1 : n2;
        String zeroStr = "";
        for (int i = 0; i < bigStr.length() - smallStr.length(); i++) {
            zeroStr = zeroStr + "0";
        }
        smallStr = zeroStr + smallStr;
        char[] smallArr = smallStr.toCharArray();
        char[] bigArr = bigStr.toCharArray();
        increaseNext = false;
        for (int i = bigStr.length() - 1; i >= 0; i--) {
            sb.append(getOneNum(Integer.parseInt(String.valueOf(bigArr[i])), Integer.parseInt(String.valueOf(smallArr[i]))));
        }

        return sb.reverse().toString();
    }

    private static String getOneNum(int n1, int n2) {
        String result;
        int sum = n1 + n2;
        if (increaseNext) {
            sum++;
        }
        if (sum > 9) {
            increaseNext = true;
            result = (sum % 10) + "";
        } else {
            result = sum + "";
            increaseNext = false;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(sum("263", "4358473"));
    }
}


