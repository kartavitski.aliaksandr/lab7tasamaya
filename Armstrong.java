import java.util.Scanner;

public class Armstrong {

    public static boolean isArmstrongNumber(int num) {
        int number = num;
        int lastNum;
        int sumKub = 0;
        int pow = (num + "").length();
        while (number != 0) {
            lastNum = number % 10;
            sumKub = (int) (sumKub + Math.pow(lastNum, pow));
            number = number / 10;
        }
        return num == sumKub;

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        for (int i = 1; i < a; i++) {
            System.out.println("Число " + i + " Армстронга " + isArmstrongNumber(i));
        }
    }

}

